
package org.ws4d.coap.rest;

import java.util.HashMap;
import java.util.Vector;

import org.ws4d.coap.interfaces.CoapChannel;
import org.ws4d.coap.interfaces.CoapRequest;
import org.ws4d.coap.messages.CoapMediaType;

/**
 * Well-Known CoRE support (draft-ietf-core-link-format-05)
 * 
 * @author Nico Laum <nico.laum@uni-rostock.de>
 * @author Christian Lerche <christian.lerche@uni-rostock.de>
 */
public class CoreResource implements CoapResource {
    private final static String uriPath = "/.well-known/core";
    private HashMap<Resource, String> coreStrings = new HashMap<Resource, String>();
    ResourceServer serverListener = null; 
    CoapResourceServer server = null;
    
    public CoreResource (CoapResourceServer server){
    	this.server = server;
    }
    
    
    /*Hide*/
    @SuppressWarnings("unused")
	private CoreResource (){
    }
    
    @Override
    public String getMimeType() {
        return null;
    }

    @Override
    public String getPath() {
        return uriPath;
    }

    @Override
    public String getShortName() {
        return getPath();
    }

    @Override
    public byte[] getValue() {
        return buildCoreString(null, null, null).getBytes();
    }

    public void registerResource(Resource resource) {
        if (resource != null) {
            StringBuilder coreLine = new StringBuilder();
            coreLine.append("<");
            coreLine.append(resource.getPath());
            coreLine.append(">");
            // coreLine.append(";ct=???");
            coreLine.append(";rt=\"" + resource.getResourceType() + "\"");
            // coreLine.append(";if=\"observations\"");
            coreStrings.put(resource, coreLine.toString());
        }
    }
    
    private String buildCoreString(String resourceType, String interfaceD, String contentT) {
    	HashMap<String, Resource> resources = server.getResources();
    	StringBuilder returnString = new StringBuilder();
    	for (Resource resource : resources.values()){
    		if ((resourceType == null || resourceType.equals(resource.getResourceType())) &&
    			(interfaceD == null || interfaceD.equals(resource.getInterfaceDescription())) &&
    			(contentT == null || contentT.equals(resource.getContentType()))) {
    			returnString.append("<");
    			returnString.append(resource.getPath());
    			returnString.append(">");

    			if (resource.getResourceType() != null) {
    				returnString.append(";rt=\"" + resource.getResourceType() + "\"");
    			}
    			if (resource.getInterfaceDescription() != null) {
    				returnString.append(";if=\"" + resource.getInterfaceDescription() + "\"");
    			}
    			if (resource.getContentType() != null) {
    				returnString.append(";ct=\"" + resource.getContentType() + "\"");
    			}
    			returnString.append(",");
    		}
    		
    	}
    	String result = returnString.toString();
        if(result.length() > 0 || result.contains(",")){
        	result = result.substring(0, result.length() -1);
        }
        
        return result;
    }

    @Override
    public byte[] getValue(Vector<String> queries) {
    	String _rt = null;
    	String _if = null;
    	String _ct = null;
		for (String query : queries) {
		    if (query.startsWith("rt=")) {
		    	_rt = query.substring(3);
		    }
		    else if(query.startsWith("if=")){
		    	_if = query.substring(3);
		    }
		    else if(query.startsWith("ct=")){
		    	_ct = query.substring(3);
		    }
		}
	
		return buildCoreString(_rt, _if, _ct).getBytes();
    }

	@Override
	public String getResourceType() {
		// TODO implement
		return null;
	}

	@Override
	public CoapMediaType getCoapMediaType() {
		return CoapMediaType.link_format;
	}

	@Override
	public void post(byte[] data) {
		/* nothing happens in case of a post */
		return;		
	}

	@Override
	public void changed() {
		
	}

	@Override
	public void registerServerListener(ResourceServer server) {
		this.serverListener = server;
	}

	@Override
	public void unregisterServerListener(ResourceServer server) {
		this.serverListener = null;
	}

	@Override
	public boolean addObserver(CoapRequest request) {
		// TODO: implement. Is this resource observeable? (should)
		return false;
	}


	@Override
	public void removeObserver(CoapChannel channel) {
		// TODO: implement. Is this resource observeable? (should)
	}


	@Override
	public boolean isObservable() {
		return false;
	}
	
	public int getObserveSequenceNumber(){
		return 0;
	}

	@Override
	public long expires() {
		/* expires never */
		return -1;
	}


	@Override
	public boolean isExpired() {
		return false;
	}


	@Override
	public String getInterfaceDescription() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getContentType() {
		// TODO Auto-generated method stub
		return null;
	}
}
